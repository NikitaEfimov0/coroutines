
import kotlinx.coroutines.delay
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking


suspend fun firstNumRet():Int{
   delay(1000)
   return 1
}

suspend fun secondNumRet():Int{
   delay(1000)
   return 2
}

suspend fun SerialExec():Double{
   val startTime = System.currentTimeMillis()
   val f = firstNumRet()
   val s =secondNumRet()
   val totalTime = (System.currentTimeMillis()-startTime).toDouble()
   println(f+s)
   return totalTime
}

suspend fun AsyncExec():Double{
   val startTime = System.currentTimeMillis()
   runBlocking {
       val f = async { firstNumRet() }
       val s = async { secondNumRet() }
       println(f.await()+s.await())
   }
   val totalTime = (System.currentTimeMillis() - startTime).toDouble()
   return totalTime
}

fun main(args:Array<String>): Unit = runBlocking {
   launch { println(SerialExec()) }
   launch { println(AsyncExec()) }
}
