

import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

fun main(args:Array<String>) = runBlocking {
   launch { // launch a new coroutine and continue
        // non-blocking delay for 1 second (default time unit is ms)
       println("World!") // print after delay
       delay(1000L)
   }
   println("Hello, ") // main coroutine continues while a previous one is delayed
   delay(2000L)
}
