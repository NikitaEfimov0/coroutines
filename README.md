# Coroutines

Репозиторий 8 этапа марафона по Android-разработке. Код заданий представлен в 3 файлах:

[1 задание](https://gitlab.com/NikitaEfimov0/coroutines/-/blob/main/Task%208/src/main/kotlin/part1.kt)
[2 задание](https://gitlab.com/NikitaEfimov0/coroutines/-/blob/main/Task%208/src/main/kotlin/part2.kt)
[3 задание](https://gitlab.com/NikitaEfimov0/coroutines/-/blob/main/Task%208/src/main/kotlin/part3.kt)

Что бы запустить проект, нужно закомментировать функцию main в остальных файлах.
